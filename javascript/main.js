// When the user scrolls down 20px from the top of the document, show the button
let limit = 600;

function scrollFunc(limit)
{
    if (document.body.scrollTop > limit || document.documentElement.scrollTop > limit)
    {
        document.getElementById("up-btn").style.display = "block";
    } else {
        document.getElementById("up-btn").style.display = "none";
    }
}
window.onscroll = function() {scrollFunc(limit)};

// When the user clicks on the button, scroll to the top of the document
function toTop()
{
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
}

// Detect JS support
document.body.className = document.body.className + " js_enabled";
//let links = document.getElementsByTagName("a");
let white = "white";
let black = "#151515";
let selectedColor;

function cookieColorSetter()
{
if (document.cookie.length > 0)
{

    if (document.cookie.includes("white"))
    {

        document.body.style.backgroundColor = white;
        document.body.style.color = "black";

    }
    else if (document.cookie.includes("#151515"))
    {

        document.body.style.backgroundColor = black;
        document.body.style.color = "white";

    }

}
}
function checkBodyColor()
{
    let table_odd_row = document.getElementsByClassName("odd-row");
    let dropdown_menu = document.getElementById("dropdown-menu");
    let darkMode_button = document.getElementsByClassName("acc-btn")[1];
    let cookieLink = document.getElementById("cookie-link");

    if (document.body.style.backgroundColor == "rgb(21, 21, 21)")
    {

        for (let i = 0; i < table_odd_row.length; i++)
        {
            table_odd_row[i].style.backgroundColor = "#0C0C0C";
        }
        document.body.style.color = white;
        darkMode_button.innerHTML = "Dark Mode: on";
        let nav = document.getElementsByTagName("nav");
        let links = nav[0].getElementsByTagName("a");
        for (let i = 0; i < links.length; i++)
        {

        links[i].style.color = white;
        cookieLink.style.color = "#08D72C";

        }

    }
}

function show()
{

    let dropdown_menu = document.getElementById("dropdown-menu");
    if (dropdown_menu.style.display === "block" || dropdown_menu.style.display === '')
    {

    dropdown_menu.style.display = "none";

    } else
    {

        dropdown_menu.style.display = "block";

    }

};
function darkMode()
{

    let CookieDate = new Date;
    CookieDate.setFullYear(CookieDate.getFullYear() +1);
    let dropdown_menu = document.getElementById("dropdown-menu");
    let darkMode_button = document.getElementsByClassName("acc-btn")[1];
    let table_odd_row = document.getElementsByClassName("odd-row");
    let cookieLink = document.getElementById("cookie-link");

    if (document.body.style.backgroundColor == white || document.body.style.backgroundColor == "white" || document.body.style.backgroundColor == "")
    {

        selectedColor = black;
        document.cookie = "color=" + selectedColor + ";" + "expires=" + CookieDate.toGMTString() + ";" + "path=/";
        document.body.style.backgroundColor = selectedColor;
        document.body.style.color = white;
        darkMode_button.innerHTML = "Dark Mode: on";
        let nav = document.getElementsByTagName("nav");
        let links = nav[0].getElementsByTagName("a");
        for (let i = 0; i < links.length; i++)
        {

        links[i].style.color = white;
        cookieLink.style.color = "#08D72C";

        };
        for (let i = 0; i < table_odd_row.length; i++)
        {
            table_odd_row[i].style.backgroundColor = "#0C0C0C";
        }

    }else
    {


        selectedColor = white;
        document.cookie = "color=" + selectedColor + ";" + "expires=" + CookieDate.toGMTString() + ";" + "path=/";
        document.body.style.backgroundColor = selectedColor;
        document.body.style.color = black;
        darkMode_button.innerHTML = "Dark Mode: off";
        let nav = document.getElementsByTagName("nav");
        let links = nav[0].getElementsByTagName("a");
        for (let i = 0; i < links.length; i++)
        {

        links[i].style.color = black;
        cookieLink.style.color = "#08D72C";

        }
        darkMode_button.style.color = selectedColor;

        for (let i = 0; i < table_odd_row.length; i++)
        {
            table_odd_row[i].style.backgroundColor = "#DCDCDC";
        }

    }

}
function projectHover(num)
{

    document.getElementsByClassName("info")[num].style.visibility = "hidden";
    let githubText = document.getElementsByClassName("content");
    githubText[num].style.opacity = "100";
    githubText[num].style.height = "50%";
    githubText[num].style.fontSize = "2em";
    githubText[num].style.position = "relative";
    githubText[num].style.zIndex = "3";
    let link = document.getElementsByClassName("link-handler");
    link[num].style.backgroundColor = "rgba(0,0,0,0.7)";

};
function projectHoverClear(num)
{

    document.getElementsByClassName("info")[num].style.visibility = "visible";
    let githubText = document.getElementsByClassName("content");
    githubText[num].style.opacity = "0";
    githubText[num].style.height = "0";
    githubText[num].style.fontSize = "0";
    let link = document.getElementsByClassName("link-handler");
    link[num].style.backgroundColor = "rgba(0,0,0,0.0)";

}
cookieColorSetter();
checkBodyColor()
